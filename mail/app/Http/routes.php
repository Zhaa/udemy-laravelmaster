<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Mail;

Route::get('/', function () {
    // return view('welcome');

    $data = [
        'title'=>'This title',
        'content'=>'This content',
    ];

    Mail::send('emails.test', $data, function($message){
        $message->from('test@mail.com')
        ->to('rezapermana.aqi@gmail.com', 'Zaza')
        ->subject('Example mail');

    });


});


// Route::get('', function () {}):
// Route::get('', function () {}):
// Route::get('', function () {}):