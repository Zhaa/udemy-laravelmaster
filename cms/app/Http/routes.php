<?php
if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_WARNING);
}

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/post', 'PostsController@index');

// Route::resource('posts', 'PostsController');

// Route::get('contact', 'PostsController@contact');
// Route::get('post/{no}/{name}/{pass}', 'PostsController@show_post');


// Database Raw SQL Queries
// Route::get('/insert', function() {
//     DB::insert('insert into posts(title, content) values(?, ?)', ['PHP with Laravel 2', 'Laravel 22']);
// });

// Route::get('/read', function () {
//     $results = DB::select('select * from posts where id = ?', [1]);

//     // foreach ($results as $result) {
//     //     return $result->title;
//     // }

//     return $results;
// });


// Route::get('/update', function () {
//     $updated = DB::update('update posts set title = "Update title" where id = ?', [1]);

//     return $updated;
// });

// Route::get('/delete', function (){
//     $deleted = DB::delete('delete from posts where id = ?', [1]);

//     return $deleted;
// });




// Eloquent
// use App\Post;
// Route::get('/find', function () {
//     // $posts = Post::all();

//     // foreach ($posts as $post) {
//     //     return $post->title;
//     // }

//     $post = Post::find(2);

//     return $post->title;
// });

// Route::get('/findwhere', function() {
//     $posts = Post::where('id', 2)->orderBy('id', 'desc')->take(1)->get();

//     return$posts;
// });

// Route::get('/findmore', function() {
//     $posts = Post::findOrFail(1);

//     return $posts;
// });

// Route::get('/basicinsert', function() {
//     $post = new Post;
//     $post->title = 'New ORM title';
//     $post->content = 'wow is a magic';
//     $post->save();
// });

// Route::get('/basicinsert2', function() {
//     $post = Post::find(2);
//     $post->title = 'New ORM title 2';
//     $post->content = 'wow is a magic 2';
//     $post->save();
// });

// Route::get('/createmass', function() {
//     Post::create(['title'=>'the create mrthod', 'content' => 'wow i am learning mass assignment']);
// });

// Route::get('/updatemass', function () {
//     Post::where('id', 2)->where('is_admin', 0)->update(['title' => 'NEW update title', 'content' => 'i will updated']);
// });

// Route::get('/deletemass', function() {
//     $post = Post::find(2);

//     $post->delete();
// });

// Route::get('deletemass2', function() {
//     Post::destroy([3,4]);
//     // Post::destroy(3);
//     // Post::where('is_admin', 0)->delete();
// });

// Route::get('/softdelete', function() {
//     Post::find(6)->delete();
// });

// Route::get('/readsoftdelete', function() {
//     // Post::find(3);

//     // return $post;

//     // $post = Post::withTrashed()->where('is_admin', 0)->get();

//     // return $post;

//     $post = Post::onlyTrashed()->where('id', 3)->get();

//     return $post;
// });


// Route::get('/restore', function() {
//     Post::withTrashed()->restore();
// });

// Route::get('/forcedelete', function() {
//     Post::onlyTrashed()->forceDelete();
// });


// Eloquent Relationship
use App\Post;
use App\User;
use App\Role;
use App\Country;
use App\Photo;
use App\Tag;
use Carbon\Carbon;

// // Has One to One relationship
// Route::get('/user/{id}/post', function($id) {
//     return User::find($id)->post;
// });

// Route::get('/post/{id}/user', function($id) {
//     return Post::find($id)->user;
// });


// Has One to Many relationship
// Route::get('/posts', function () {
//     $user = User::find(1);
    
//     foreach($user->posts as $post) {
//         echo $post->title . "<br>";
//     }
// });



// Has Many to Many relationship
// Route::get('/user/{id}/role', function($id) {
    // $user = User::find($id);

    // foreach ($user->roles as $role){
    //     return $role->name;
    // }

    // $user = User::find($id)->roles()->orderBy('id', 'desc')->get();

    // return $user;
// });



// Accessing the intermediate table/pivot
// Route::get('user/pivot', function() {
//     $user = User::find(1);

//     foreach ($user->roles as $role) {
//         return $role->pivot->created_at;
//     }
// });

// Route::get('/user/country', function () {
//     $country = Country::find(2);

//     foreach($country->posts as $post){
//         return $post->title;
//     }
// });




// Polymorphic Relations
// Route::get('user/photos', function() {
//     // $user = User::find(1);

//     // foreach( $user->photos as $photo) {
//     //     return $photo;
//     // }

//     $post = Post::find(1);

//     foreach($post->photos as $photo) {
//         echo $photo->path."<br>";
//     }
// });

// Route::get('post/{id}/photos', function($id) {
//     $post = Post::find($id);

//     foreach($post->photos as $photo) {
//         echo $photo->path."<br>";
//     }
// });

// Route::get('photo/{id}/post', function($id){
//     $photo = Photo::findOrFail($id);

//     return $photo->imageable;
// });




// Polymorphic Many to Many
// Route::get('/post/tag', function() {
//     $post = Post::find(1);

//     foreach($post->tags as $tag) {
//         echo $tag->name;
//     }
// });

// Route::get('/tag/post', function() {
//     $tag = Tag::find(2);

//     foreach($tag->posts as $post) {
//         echo $post->title;
//     }
// });


// Crud Apps
// Note: learn udemy section 19, 135 tidak perlu menggunakan middleware
Route::resource('posts', 'PostsController');

Route::get('dates', function() {
    $date =  new DateTime('+1 week');

    echo $date->format('m-d-Y');

    echo '<br>'; 

    echo Carbon::now()->addDays(10)->diffForHumans();

    echo '<br>';

    echo Carbon::now()->subMonths(5)->diffForHumans();

    echo '<br>';

    echo Carbon::now()->yesterday()->diffForHumans();

    echo '<br>';
});

Route::get('getname', function() {
    $user =  User::find(1);

    echo $user->name;
});

Route::get('setname', function() {
    $user =  User::find(1);

    $user->name = "william";

    $user->save();
});
// Route::get('', function() {});
// Route::get('', function() {});
// Route::get('', function() {});
// Route::get('', function() {});
// Route::get('', function() {});
// Route::get('', function() {});

















Route::group(['middleware' => ['web']], function () {

});