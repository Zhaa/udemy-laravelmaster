@extends('layouts.app')

@section('content')
    <h1>Contact Page</h1>

    @if (count($org))
        @foreach($org as $orang)
            <li>{{$orang}}</li>
        @endforeach
    @endif
@endsection

@section('footer')
    <script>
        alert();
    </script>
@endsection