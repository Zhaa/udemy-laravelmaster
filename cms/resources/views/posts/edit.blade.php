@extends('layouts.app')

@section('content')
    <h1>Edit</h1>

    {{-- {!! Form::open(['action'=>['PostsController@update', $post->id],'method'=>'PUT']) !!} --}}

    {!! Form::model($post, ['action'=>['PostsController@update', $post->id],'method'=>'PUT']) !!}
    <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', $post->title, ['class'=>'form-control'])!!}
    </div>
        {!! Form::submit('UPDATE', ['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
    

    {!! Form::open(['action'=>['PostsController@destroy', $post],'method'=>'DELETE']) !!}
    {!! Form::submit('DELETE', ['class'=>'btn btn-delete']) !!}
    {!! Form::close() !!}
@endsection